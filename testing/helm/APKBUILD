# Contributor: techknowlogick <techknowlogick@gitea.io>
# Maintainer: techknowlogick <techknowlogick@gitea.io>
pkgname=helm
pkgver=3.7.0
pkgrel=0
pkgdesc="The Kubernetes Package Manager"
url="https://helm.sh/"
arch="all !mips !mips64 !armhf" # mips does not have go, unit tests fails with armhf
license="Apache-2.0"
makedepends="bash go"
options="net"
subpackages="$pkgname-bash-completion $pkgname-fish-completion $pkgname-zsh-completion"
source="$pkgname-$pkgver.tar.gz::https://github.com/helm/helm/archive/v$pkgver.tar.gz"

# secfixes:
#   3.6.0-r0:
#     - CVE-2021-21303
#   3.6.1-r0:
#     - CVE-2021-32690

case "$CARCH" in
	# Disable check on 32bit systems due to upstream test "TestPlatformPrepareCommand" that does not account for these platforms
	x86|armv7) options="$options !check" ;;
esac

export GOPATH="$srcdir/go"
export GOCACHE="$srcdir/go-build"
export GOTEMPDIR="$srcdir/go"

export GOFLAGS="$GOFLAGS -modcacherw"

prepare() {
	default_prepare

	# Update procfs version for riscv64 support.
	go mod edit -replace \
		github.com/prometheus/procfs=github.com/prometheus/procfs@v0.6.0
	go mod download github.com/prometheus/procfs
}

build() {
	export CGO_ENABLED=0 # breaks on aarch64
	make GOFLAGS="$GOFLAGS"

	./bin/helm completion bash > $pkgname.bash
	./bin/helm completion fish > $pkgname.fish
	./bin/helm completion zsh > $pkgname.zsh
}

check() {
	make test-unit GOFLAGS="$GOFLAGS"
}

package() {
	install -Dm755 bin/helm -t "$pkgdir/usr/bin"

	install -Dm644 $pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish "$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -Dm644 $pkgname.zsh "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
8e763308e2f4dcf73b2bcd3af22ed4fc1fd68603fa1edc5118ff6b1c401a0c85a50bb2e934e5bdefff2626778657c7da62e620982965edb5217bbef3f040433c  helm-3.7.0.tar.gz
"
