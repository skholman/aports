# Contributor: Konstantin Kulikov <k.kulikov2@gmail.com>
# Maintainer: Konstantin Kulikov <k.kulikov2@gmail.com>

# Make sure to keep this package in sync with grafana.

# Frontend files split from main grafana package.
# This is because grafana's build process is
# > build backend -> test backend -> build frontend -> test frontend
# while alpine doesn't allow easily interleaving those.
# Second problem is grafana doesn't support building on anything except linux/amd64,
# while alpine requires every build to be done natively.
# This is also a reason why we use prebuilt frontend archive.

pkgname=grafana-frontend
pkgver=8.1.7
pkgrel=0
pkgdesc="Open source, feature rich metrics dashboard and graph editor (frontend files)"
url="https://grafana.com"
arch="noarch"
license="AGPL-3.0-only"
options="!check" # We don't build frontend from sources.
source="$pkgname-$pkgver-bin.tar.gz::https://dl.grafana.com/oss/release/grafana-$pkgver.linux-amd64.tar.gz"
builddir="$srcdir/grafana-$pkgver"

package() {
	install -dm755 "$pkgdir/usr/share/grafana"
	cp -r "$builddir/plugins-bundled" "$builddir/public" "$pkgdir/usr/share/grafana/"
}

sha512sums="
5f32601db056b29ece2dae28b19547a7eb93b2068cf8cf17dfb743a83d326f00689375be7bec3e4fb08d215a3b2387565d338c5943e29e2664ec93482b157494  grafana-frontend-8.1.7-bin.tar.gz
"
